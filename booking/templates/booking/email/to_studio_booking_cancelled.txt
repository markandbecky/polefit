{{ booking.user.first_name }} {{ booking.user.last_name }} ({{ booking.user.username }}) has cancelled a booking for {{ event }}.

{% if booking.paid %}
Please review and refund payment for this booking.

When the payment has been refunded, use the link below to confirm and update payment status in the system:
{{ host }}/pf_admin/ipn/paypalipn/confirm-refunded/{{ booking.id }}
{% endif %}

{% include 'account/email/footer.txt' %}
